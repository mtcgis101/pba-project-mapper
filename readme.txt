///// Plan Bay Area Interactive Project Map /////

--- Project Description ---

The Plan Bay Area interactive project map is a tool that allows uses to see and explore the Bay Area’s 100 largest transportation projects. This map is designed to help visualize the major transportation investments outlined in the currently adopted Plan Bay Area, approved by MTC and ABAG in July 2013.
The map displays information by project category—road pricing, highway, and transit—and allows users to choose whether to view these projects on top of street-level, region-centric or satellite maps. A general project description, along with more in-depth information about the project’s location, sponsor, expected completion date and cost, is provided. A photo and rendering gallery also is included for many projects.

--- Brief explanation about development process and usage of language and libraries ---

This application is designed using HTML5, CSS3 and JavaScript. JavaScript library, "D3" and "Leaflet" are used for the main part of the application as an interactive map. 

All geographic data (Bay Area's county line, county limits, Priority Development Area, Urbanized Area, Rails, Highway/Freeway, county name labels and Plan Bay Area projects data), which are layered on the base maps (ESRI's Gray, Street, Imagery) are provided by MTC GIS with the form of shapefile.
The shapefiles are converted to GeoJSON using GDAL (http://www.gdal.org/).

Using D3, these shapefiles are rendered as svg on the Leaflet.


---Deployment Information ---

The Plan Bay Area interactive project map is deployed within the AWA Elastic Beanstalk Environment.  The url for the application is:
http://projectmapper.elasticbeanstalk.com/

This location may change in the near term future.

--- File structure ---

index.ejs
js > main.js(main JavaScript file), map.js (this contains the javaScript relates to map generation), lib(this folder contains all javaScript libraries/plugins used in this application)
css > main.css(main css file), fonts.css(web font), plugins(contains separate css files used by libraries/plugins), fonts(web font file)
data > rtpdata.js and RTPdata.csv(contains all projects data, such as project description, cost and timeline)
mapData(contains all GeoJSON file)
images (images used in this application)
projectImages (project images)


--- Note ---

- There is an issue to render SVG file using D3 in Leaflet. It doesn't simply render objects in GeoJSON file (for instance, if the date is point data, only points data), instead they create like transparent layer, which cover entire are which objects are spread out (for instance, the case of point date spread to north, east, south west, the layer covers entire area as each point as an edge).
  This is not an issue if the application only project map data without interactivity. However, if we need to make the map interactive, this transparent layer blocks objects below the layer and make them unaccessible (unclickable). To solve this issue, project data is combined to one single GeoJSON file manually (originally separated by point/line/polygon). 
  
- Depending on the browsers, the timing to load GeoJSON file was different. Some browser read less expensive GeoJSON file fast. Handling this issue, all GeoJSON files are contained to an array and wrote the program to read GeoJSON file from this array.


--- Designer/developer Info --

Kenji Wada | email@kenjiwada.jp



All data relate to the Project Mapper is archived within the GIS Server at the following directory:
\_Section\LPA\Major_Projects

Following files/folders are important

- PlanBayArea_interactive_project_map040115 (final version of the application)
- PBA_interactivemap_readme.txt (External use read me text for the application)
- memo_internaluse.txt (This text, for internal use to explain where the data is)
- Shapefile (Contains all shapefiles used for the project)
- texts_spreadsheets (Texts and spreadsheets used for the project, all texts in the application are proofred by Leslie Lara (LPA) and Kristina Wenzinger provided introduction for the applicatio)

Note:
The projects to show on the application were determined by Dave Vautin (PL) as it can be found from the spreadsheet "All_PBAComparison.xlsx."
As I notice, the application is not rendered appropriately on some older versions of Safari and Internet Explorer, which cannot take "vh/vw(view height/view width)" in CSS.
I noticed it happens sometime that uses cannot click on the projects on the map if their internet connection is slow or they use low performance computer.  

NodeJS Notes:

1. Installed Libraries
	MSSQL -- SQL Server driver
	MongoJS -- MongoDB driver -- N/A
	Compression -- Gzip compression of server content


2. Middleware
	EnableCORS -- Allow cross-browser requests

3. Templating 
	EJS -- Embedded Javascript (in the /views directory)
	HTML -- HTML files can be placed in the /public directory

4. Main.js
	Renamed from default app.js for Elastic Beanstalk compatibility