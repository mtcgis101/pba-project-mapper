///// Plan Bay Area Interactive Project Map /////


--- Project Description ---

The Plan Bay Area interactive project map is a tool that allows uses to see and explore the Bay Area’s 100 largest transportation projects. This map is designed to help visualize the major transportation investments outlined in the currently adopted Plan Bay Area, approved by MTC and ABAG in July 2013.
The map displays information by project category—road pricing, highway, and transit—and allows users to choose whether to view these projects on top of street-level, region-centric or satellite maps. A general project description, along with more in-depth information about the project’s location, sponsor, expected completion date and cost, is provided. A photo and rendering gallery also is included for many projects.

--- Brief explanation about development process and usage of language and libraries ---

This application is designed using HTML5, CSS3 and JavaScript. JavaScript library, "D3" and "Leaflet" are used for the main part of the application as an interactive map. 

All geographic data (Bay Area's cast line, county limits, Priority Development Area, Urbanized Area, Rails, Highway/Freeway, county name labels and Plan Bay Area projects data), which are layered on the base maps (ESRI's Gray, Street, Imagery) are provided by MTC GIS with the form of shapefile.
The shapefiles are converted to GeoJSON using GDAL (http://www.gdal.org/).

Using D3, these shapefiles are rendered as svg on the Leaflet.


--- File structure ---

index.html
js > main.js(main JavaScript file), map.js (this contains the javaScript relates to map generation), lib(this folder contains all javaScript libraries/plugins used in this application)
css > main.css(main css file), fonts.css(web font), plugins(contains separate css files used by libraries/plugins), fonts(web font file)
data > rtpdata.js and RTPdata.csv(contains all projects data, such as project description, cost and timeline)
mapData(contains all GeoJSON file)
images (images used in this application)
projectImages (project images)


--- Note ---

- There is an issue to render SVG file using D3 in Leaflet. It doesn't simply render objects in GeoJSON file (for instance, if the date is point data, only points data), instead they create like transparent layer, which cover entire are which objects are spread out (for instance, the case of point date spread to north, east, south west, the layer covers entire area as each point as an edge).
  This is not an issue if the application only project map data without interactivity. However, if we need to make the map interactive, this transparent layer blocks objects below the layer and make them unaccessible (unclickable). To solve this issue, project data is combined to one single GeoJSON file manually (originally separated by point/line/polygon). 
  
- Depending on the browsers, the timing to load GeoJSON file was different. Some browser read less expensive GeoJSON file fast. Handling this issue, all GeoJSON files are contained to an array and wrote the program to read GeoJSON file from this array.


--- Designer/developer Info --

Kenji Wada | email@kenjiwada.jp









